import json

import pytest


@pytest.fixture
def lambda_handler():
    import cfn_custom_resource

    def func(e, c):
        return None, None

    cfn_custom_resource.create()(func)
    cfn_custom_resource.update()(func)
    cfn_custom_resource.delete()(func)
    return cfn_custom_resource.lambda_handler


# https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/crpg-ref-requests.html
@pytest.fixture(params=['Create', 'Update', 'Delete'])
def incoming(request, httpserver):
    """
    A fixture that represents an incoming request to the custom resource lambda
    """
    httpserver.serve_content('OK', 200, headers=[''])
    o = {
            'RequestType': request.param,
            'ResourceType': 'default',
            'StackId': 'stackid',
            'LogicalResourceId': 'logicalresourceid',
            'RequestId': 'requestid',
            'ResponseURL': httpserver.url,
            'ResourceProperties': {}
    }
    if request.param != 'Create':
        o['PhysicalResourceId'] = 'physicalresourceid'

    if request.param == 'Update':
        o['OldResourceProperties'] = {}

    return o


def test_basic(lambda_handler, incoming, httpserver):
    lambda_handler(incoming, None)

    req = json.loads(httpserver.requests[0].data)
    assert req['Status'] == 'SUCCESS', 'Returned status is not SUCCESS'
    assert req['RequestId'] == incoming['RequestId'], 'RequestId should stary the same'
    assert req['LogicalResourceId'] == incoming['LogicalResourceId'], 'LogicalResourceId should stay the same'
    assert req['StackId'] == incoming['StackId'], 'StackId should stay the same'

    if incoming['RequestType'] != 'Create':
        assert req['PhysicalResourceId'] == incoming['PhysicalResourceId'], 'PhysicalResourceId should stay the same'
    else:
        assert 'PhysicalResourceId' in req
